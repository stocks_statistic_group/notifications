package net.proselyte.notifications.utils;

import net.proselyte.notifications.dto.NotificationProducerDto;
import net.proselyte.notifications.entity.Notification;
import net.proselyte.notifications.entity.Status;
import net.proselyte.notifications.entity.Type;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", imports = {Status.class, Type.class})
public interface MapperDto {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "status", expression = "java(Status.valueOf(dto.getStatus()))"),
            @Mapping(target = "type", expression = "java(Type.valueOf(dto.getType()))"),
            @Mapping(target = "created", source = "dto.created"),
            @Mapping(target = "modified", source = "dto.created"),
            @Mapping(target = "createdBy", source = "dto.createdBy"),
            @Mapping(target = "modifiedBy", source = "dto.createdBy"),
            @Mapping(target = "body", source = "dto.body")
    })
    Notification notificationProducerDtoToNotification(NotificationProducerDto dto);
}
