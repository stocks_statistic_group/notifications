package net.proselyte.notifications.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;

@Data
@Document
public class Notification {

    @Id
    private String id;
    private Status status;
    private Type type;
    private LocalDateTime created;
    private LocalDateTime modified;
    private String createdBy;
    private String modifiedBy;
    private Object body;
}
