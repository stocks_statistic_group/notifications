package net.proselyte.notifications.job;

import lombok.RequiredArgsConstructor;
import net.proselyte.notifications.dto.NotificationProducerDto;
import net.proselyte.notifications.service.NotificationService;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class QueueListener {

    private final NotificationService notificationService;

    @SqsListener(value = "${sqs.queueName}", deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    public void receiveMessage(NotificationProducerDto notificationProducerDto) {
        notificationService.save(notificationProducerDto);
    }
}
