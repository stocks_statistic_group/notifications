package net.proselyte.notifications.service;

import lombok.RequiredArgsConstructor;
import net.proselyte.notifications.dto.NotificationProducerDto;
import net.proselyte.notifications.repository.NotificationRepository;
import net.proselyte.notifications.utils.MapperDto;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationService {

    private final NotificationRepository repository;
    private final MapperDto mapperDto;

    public void save(NotificationProducerDto notificationProducerDto) {
        var notification = mapperDto.notificationProducerDtoToNotification(notificationProducerDto);
        repository.insert(notification);
    }
}
