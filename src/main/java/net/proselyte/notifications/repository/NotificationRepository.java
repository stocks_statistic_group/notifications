package net.proselyte.notifications.repository;

import net.proselyte.notifications.entity.Notification;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NotificationRepository extends MongoRepository<Notification, String> {

}
