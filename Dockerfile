FROM openjdk:11

RUN mkdir -p /usr/local/src
COPY . /usr/local/src
WORKDIR /usr/local/src
RUN ./gradlew build
ARG JAR_FILE=/usr/local/src/build/libs/*SNAPSHOT.jar
RUN mkdir -p /usr/local/notifications
RUN cp ${JAR_FILE} /usr/local/notifications/app.jar
RUN rm -r /usr/local/src
WORKDIR /usr/local/notifications

ARG AWS_SQS_ACCESS_KEY
ARG AWS_SQS_SECRET_KEY
ARG AWS_SQS_REGION
ARG AWS_SQS_URI
ARG AWS_SQS_QUEUE_NAME
ARG MONGODB_USERNAME
ARG MONGODB_PASSWORD
ARG MONGODB_DATABASE
ARG MONGODB_PORT
ARG MONGODB_HOST

ENV AWS_SQS_ACCESS_KEY=${AWS_SQS_ACCESS_KEY}
ENV AWS_SQS_SECRET_KEY=${AWS_SQS_SECRET_KEY}
ENV AWS_SQS_REGION=${AWS_SQS_REGION}
ENV AWS_SQS_URI=${AWS_SQS_URI}
ENV AWS_SQS_QUEUE_NAME=${AWS_SQS_QUEUE_NAME}
ENV MONGODB_USERNAME=${MONGODB_USERNAME}
ENV MONGODB_PASSWORD=${MONGODB_PASSWORD}
ENV MONGODB_DATABASE=${MONGODB_DATABASE}
ENV MONGODB_PORT=${MONGODB_PORT}
ENV MONGODB_HOST=${MONGODB_HOST}

RUN mkdir -p /usr/local/stocks_statistic_api
WORKDIR /usr/local/stocks_statistic_api
COPY ${JAR_FILE} /usr/local/stocks_statistic_api/app.jar
ENTRYPOINT java -jar app.jar \
      --aws.sqs.access-key=${AWS_SQS_ACCESS_KEY} \
      --aws.sqs.secret-key=${AWS_SQS_SECRET_KEY} \
      --aws.sqs.region=${AWS_SQS_REGION} \
      --aws.sqs.uri=${AWS_SQS_URI} \
      --aws.sqs.queue-name=${AWS_SQS_QUEUE_NAME} \
      --mongodb.username=${MONGODB_USERNAME} \
      --mongodb.password=${MONGODB_PASSWORD} \
      --mongodb.database=${MONGODB_DATABASE} \
      --mongodb.port=${MONGODB_PORT} \
      --mongodb.host=${MONGODB_HOST}