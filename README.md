
#AWS
AWS Simple Queue Service (SQS) - create a queue with the name "notification"
AWS Identity and Access Management (IAM) - create  User, Permissions policies: AmazonEC2FullAccess, AdministratorAccess

#User settings in application.yaml
access-key, secret-key, region.static, end-point.uri

#MongoDB
docker-compose -f docker-compose.yaml up -d
http://localhost:8081/ -> create database "notifications"